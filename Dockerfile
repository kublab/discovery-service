FROM openjdk:8-jre-alpine

VOLUME /tmp
EXPOSE 8761

ADD ./gitlab/wars/discovery-service-0.0.1-SNAPSHOT.war /app.war
